const Loader = () => {
  return (
    <div className="d-flex justify-content-center " style={{marginTop:'20%'}}>
      <div className="spinner-border text-primary" role="status">
        <span className="visually-hidden">Загрузка...</span>
      </div>
    </div>
  )
}
export default Loader
