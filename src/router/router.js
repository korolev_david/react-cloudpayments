import React from 'react'
import { Route, Switch } from 'react-router'
import ErrorApi from '../container/errorApi/errorApi'
import Loader from '../component/loader'
import Main from '../container/main/main'
import MainPaymentOnClick from '../container/mainPaymentOnClick/mainPaymentOnClick'
export const useRouters = (asyncProps) => {
  const getPagePayment = () => {
    if (asyncProps.loader) {
      return <Loader />
    } else if (asyncProps.error) {
      return <ErrorApi />
    } else {
      return <Main />
    }
  }
  const getPageOnClick = () => {
    if (asyncProps.loader) {
      return <Loader />
    } else if (asyncProps.errorProduct) {
      return <ErrorApi />
    } else {
      return <MainPaymentOnClick/>
    }

  }
  return (
    <Switch>
      <Route path="/" exact>{getPagePayment}</Route>
      <Route path="/product" exact>
        {getPageOnClick}
      </Route>
    </Switch>
  )
}
