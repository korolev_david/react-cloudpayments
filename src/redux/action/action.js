import {
  GET_DATA_ORDER,
  GET_DATA_ORDER_ERROR,
  GET_PRODUCT_DATA,
  GET_DATA_ERROR,
  LOADER_HIDE,
  NEXT_FETCH,
  GET_DATA_ONCLICK,
  NEXT_STAGE,
} from './type'

export function nextStage(stageId) {
  return (dispatch) => {
    dispatch({
      type: NEXT_STAGE,
      payload: stageId,
    })
  }
}


export function getProductData() {
  return async (dispatch) => {
    try {
      const windowUrl = window.location.search
      const params = new URLSearchParams(windowUrl)
      const id = params.get('id')
      if(id === null)dispatch({ type: GET_DATA_ERROR })
      const response = await fetch(
        'https://siwitpro.com/api/payment/getDataProduct.php?id=' + id,
      )
      const json = await response.json()
      if (response.status === 200) {
        
          dispatch({ type: GET_PRODUCT_DATA, payload: json })
          dispatch({ type: LOADER_HIDE })   
      } else {
        dispatch({ type: GET_DATA_ERROR })
        dispatch({ type: LOADER_HIDE })
      }
    } catch {
      dispatch({ type: GET_DATA_ERROR , payload: false })
      dispatch({ type: LOADER_HIDE })
    }
  }
}
export function creatDataUser(name, phone, email, productId) {
  return async (dispatch) => {
    try {
      const url =
        'https://siwitpro.com/purnov/api/receiver_payment.php?name =' +
        name +
        '&phone=' +
        phone +
        '&email=' +
        email +
        '&p_id=' +
        productId
      const response = await fetch(url)
      if (response.status === 200) {
        dispatch({ type: NEXT_FETCH, payload: 'Формирование данных...' })
        const responseTask = await fetch(
          'https://siwitpro.com/api/payment/getTaskData.php?id=' +
            productId +
            '&phone=' +
            phone +
            '&email=' +
            email,
        )
        const taskJson = await responseTask.json()
        if (responseTask.status === 200) {
          let set_paid = 0
          let description = ''
          let nameProduct = ''
          let total_cost_sale = 0
          let total_cost = 0
          let order_id = 0
          if (taskJson.products.length !== 0)
            for (let value of taskJson.products) {
              if (
                value.description !== false &&
                value.description !== undefined
              ) {
                description += value.description + ' '
              }

              nameProduct = value.productName + ' '
              total_cost_sale =
                Number(total_cost_sale ? total_cost_sale : 0) +
                Number(value.sum_sale)
              total_cost =
                Number(total_cost ? total_cost : 0) + Number(value.sum)
              order_id = value.orderID
            }
          set_paid = total_cost_sale - taskJson.paid
          taskJson.set_paid = set_paid
          taskJson.description = description
          taskJson.nameProduct = nameProduct
          taskJson.total_cost_sale = total_cost_sale
          taskJson.total_cost = total_cost
          taskJson.order_id = order_id
          dispatch({ type: GET_DATA_ONCLICK, payload: taskJson })
          if (set_paid === 0) {
            dispatch({
              type: NEXT_STAGE,
              payload: 4,
            })
          } else {
            dispatch({
              type: NEXT_STAGE,
              payload: 2,
            })
          }
        } else {
          dispatch({ type: GET_DATA_ERROR })
        }
      } else {
        dispatch({ type: GET_DATA_ERROR })
      }
    } catch {
      dispatch({ type: GET_DATA_ERROR })
    }
  }
}

export function getDataOrder() {
  return async (dispatch) => {
    try {
      const windowUrl = window.location.search
      const params = new URLSearchParams(windowUrl)
      const id = params.get('id')

      const response = await fetch(
        'https://siwitpro.com/api/payment/get-order-data.php?id=' + id,
      )
      //
      const json = await response.json()
      if (response.status === 200) {
        dispatch({ type: GET_DATA_ORDER, payload: json })
        dispatch({ type: LOADER_HIDE })
      } else {
        dispatch({ type: GET_DATA_ORDER_ERROR })
        dispatch({ type: LOADER_HIDE })
      }
    } catch {
      dispatch({ type: GET_DATA_ORDER_ERROR, payload: false })
      dispatch({ type: LOADER_HIDE })
    }
  }
}
