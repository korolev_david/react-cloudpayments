import {
  GET_DATA_ERROR,
  GET_DATA_ORDER_ERROR,
  LOADER_HIDE,
  LOADER_SHOW,
  NEXT_FETCH,
  NEXT_STAGE,
} from '../action/type'
const initialState = {
  loader: true,
  textLoader: 'Получение информации о пользователи...',
  stage: 0,
  errorProduct:false,
  error:false
}

export const appReducer = (state = initialState, action) => {
  switch (action.type) {
    case LOADER_SHOW:
      return { ...state, loader: true }
    case LOADER_HIDE:
      return { ...state, loader: false }
    case NEXT_FETCH:
      return { ...state, textLoader: action.payload }
    case NEXT_STAGE:
      return { ...state, stage: action.payload }
    case GET_DATA_ERROR:
      return { ...state, errorProduct: true }
    case GET_DATA_ORDER_ERROR:
            return {...state, error:true}

    default:
      return state
  }
}
