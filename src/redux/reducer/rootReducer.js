import { combineReducers } from "redux"
import { apiReducer } from "./apiReducer"
import { appReducer } from "./appReducer"
    export const rootReducer = combineReducers({
        appData: appReducer,
        apiData:apiReducer
    })