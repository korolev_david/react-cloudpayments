import { GET_DATA_ONCLICK, GET_DATA_ORDER, GET_PRODUCT_DATA } from "../action/type"

const initialState = {
    data:[],
    dataOnClick:[]
}

export const  apiReducer =(state = initialState, action)=>{
    switch(action.type)
    {
        case GET_DATA_ORDER:
            return {...state, data: action.payload, error: false}
        case GET_PRODUCT_DATA:
            return {...state, data: action.payload,errorProduct:false}     
        case GET_DATA_ONCLICK:
            return  {...state, dataOnClick: action.payload,errorProduct:false} 
        default:
        return state
    }
}