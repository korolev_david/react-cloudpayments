import React from 'react'
import ReactDOM from 'react-dom'
import App from './App'
import { applyMiddleware, compose, createStore } from 'redux'
import reportWebVitals from './reportWebVitals'
import { rootReducer } from './redux/reducer/rootReducer'
import thunk from 'redux-thunk'
import { Provider } from 'react-redux'
import {  HashRouter } from 'react-router-dom'
// window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()

const store = createStore(
  rootReducer,
  compose(
    applyMiddleware(thunk)
  ),
)

ReactDOM.render(
  <HashRouter>
    <Provider store={store}>
      <App />
    </Provider>
  </HashRouter>,
  document.getElementById('root'),
)

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals()
