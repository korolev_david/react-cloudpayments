import React from 'react'
import { Component } from 'react'
import { connect } from 'react-redux'
import InfoPayment from '../infoPayment/infoPayment'
import InfoProduct from '../infoProduct/infoProduct'
import InfoUser from '../infoUser/infoUser'
import Payment from '../payment/payment'
import SuccesPayment from '../succesPayment/succesPayment'
import './style.css'

class Main extends Component {
  
  constructor(props) {
    super(props)

    this.state = {
      stage: 0,
      description:''
    }

    for (let value of this.props.data.products) {
      this.state.order_id = value.orderID

      if(value.description !== false && value.description !== undefined)
      {
        this.state.description += value.description+" "
      }
    

      this.state.nameProduct = value.productName+" ";
      this.state.total_cost_sale =
        Number(this.state.total_cost_sale ? this.state.total_cost_sale : 0) +
        Number(value.sum_sale)

      this.state.total_cost =
        Number(this.state.total_cost ? this.state.total_cost : 0) +
        Number(value.sum)
    }
    if(this.state.description === undefined)
    this.state.description =false
    this.state.discount =
      Number(this.state.total_cost) -
      (Number(this.state.total_cost) - Number(this.state.total_cost_sale))

    this.state.set_paid = this.state.residue =
      Number(this.state.total_cost_sale) - Number(this.props.data.paid)

    
  }
  componentDidMount()
  {
    if(this.state.set_paid === 0)
    {
      this.setState({
        stage:4
      })
    }
  }
  
  paymentChange=()=>
  {
    const widget = new global.cp.CloudPayments();
    widget.charge(
      {
        // options
        publicId: "pk_7dafd51cc7bf1fdd6f965fc36e391", //id из личного кабинета
        description: this.state.nameProduct, //назначение
        amount: this.state.set_paid, //сумма
        currency: "RUB", //валюта
        invoiceId: this.state.order_id, //номер заказа  (необязательно)
        accountId:this.props.data.email, //идентификатор плательщика (необязательно)
        skin: "modern", //, дизайн виджета
        //data: {
        //  myProp: 'myProp value' //произвольный набор параметров
        //}
      },
       (options)=> {
          this.setState({
          stage:4
        })
      },
      function (reason, options) {
        // fail
        //действие при неуспешной оплате
      }
    )
 
  }
  
  paidChangeHandelr = (event) => {
    let inputData = event.target.value
    if (!Number(inputData)) {
      event.target.value = null
    } else {
      if (this.state.residue < Number(inputData)) {
        event.target.value = inputData = this.state.residue
      }

      this.setState((prev) => {
        return {
          set_paid: Number(inputData),
        }
      })
    }
  }
  SetStage = (event)=>
  {
   
    this.setState({
      stage:event.target.value
    })
    
  }
  getPage = () => {
    
    switch (Number( this.state.stage)) {
      case 0:
        return (
          <div>
            <InfoProduct
              data={this.props.data}
              orderId={ 'Счет № '+this.state.order_id}
              total_cost={this.state.total_cost}
            />
            <InfoPayment
              paidChangeHandelr={this.paidChangeHandelr}
              total_cost_sale={this.state.discount}
              paid={this.props.data.paid}
              set_paid={this.state.residue}
              SetStage= {this.SetStage}
            />
          </div>
        )
        case 1:
          return(
          <div><InfoUser phone={this.props.data.phone} email = {this.props.data.email} name ={this.props.data.name_user} SetStage={this.SetStage}/></div>
        )
        case 2:
          return(
            <Payment set_paid={this.state.set_paid} phone={this.props.data.phone} email = {this.props.data.email} paymentChange = {this.paymentChange} SetStage= {this.SetStage}/>
          )
          case 4:
            return(
              <SuccesPayment description={this.state.description} phone={this.props.data.phone}/>
            )

      default:
        return <div></div>
    }
  }

  render() {
    const styleStep= 'step'
    const styleStepActiv = 'step active'
    const styleFaCheck = 'fa fa-check'
    return (
      <div className="container">
        <div className="track col-md-8 col-xs-12 col-sm-10 ">
          <div className={this.state.stage < 1 ? styleStep : styleStepActiv}>
            <span className="icon">
              <i className={this.state.stage < 1 ? 'fa fa-cart-plus' :styleFaCheck}></i>
            </span>
            <span className="text">Заказ</span>
          </div>
          <div className={this.state.stage < 2 ? styleStep : styleStepActiv}>
            <span className="icon">
              <i className={this.state.stage < 2 ? 'fa fa-address-card' :styleFaCheck}></i>
            </span>
            <span className="text">Данные покупателя</span>
          </div>
          <div className={this.state.stage < 3 ? styleStep : styleStepActiv}>
            <span className="icon">
              <i className={this.state.stage < 3 ? 'fa fa-credit-card' :styleFaCheck}></i>
            </span>
            <span className="text">Оплата</span>
          </div>
          <div className={this.state.stage < 4 ? styleStep : styleStepActiv}>
            <span className="icon">
              <i className={this.state.stage < 4 ? 'fa fa-check-square' :styleFaCheck}></i>
            </span>
            <span className="text">Иформация о получении</span>
          </div>
        </div>
        {this.getPage()}
      </div>
    )
  }
}
const mapStateToProps = (state) => ({
  data: state.apiData.data,
})
export default connect(mapStateToProps, null)(Main)
