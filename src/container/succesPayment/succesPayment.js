import Parser from 'html-react-parser'
const SuccesPayment = (props) => {
  return (
    <div
      className="row col-md-8 col-xs-12 col-sm-10 mt-1 mx-auto mb-3 border p-4 order justify-content-center"
      style={{ background: '#eee' }}
    >
      <h2 className="text-center">Успешная оплата</h2>
      <hr className="my-2"></hr>
      <div className="container">
        <p className="text-center">
          {' '}
          Материалы курса доступны для Вас в программе Личный Кабинет.<br></br>
          Осталось 4 простых шага.
        </p>
        <ul>
          <li>
            <a href="https://purnov.com/purnov/program/download/Setup.exe">
              Скачать Личный кабинет
            </a>
          </li>
          <li>Установить программу и начать изучение.</li>
          <li>
            Пройти предварительное тестирование связи и консультацию у
            технического специалиста Школы (только для Участников онлайн
            курсов).
          </li>
        </ul>
        <hr className="my-2"></hr>
        <p className="text-center fw-bold">
          Данные для авторизации в Личном кабинет
        </p>

        <div className="row justify-content-around">
          <div className="col text-center">
            <p>Телефон:</p>
          </div>
          <div className="col text-center">
            <p>{props.phone}</p>
          </div>
        </div>
        <div className="row justify-content-around">
          <div className="col text-center">
            <p>Пароль:</p>
          </div>
          <div className="col text-center">
            <p>12345 или пароль установленный вами ранее</p>
          </div>
        </div>
        <hr className="my-2"></hr>
        {props.description === false ? (
          <div></div>
        ) : (
          <div>
            {' '}
            <p className=" mt-5 mb-5">{Parser(props.description)}</p>
            <hr className="my-2"></hr>
          </div>
        )}

        <p className="text-center fw-bold">
          Внимание! Если Вы оплатили онлайн курс в качестве Участника, то
          обязательно ознакомьтесь с информацией ниже:
        </p>
        <p className="text-center fw-bold">Участие в уроках</p>
        <p className="fs-6 fw-light">
          Для участия в онлайн-уроках, Вам необходимо установить Zoom и пройти
          тестирование.
          <br />
          <br /> Отправить заявку на тестирование и ознакомиться с инструкцией
          по установке ZOOM можно по ссылке-
          <a href="https://forms.gle/SFAKzJg45CTyjhiH6">
            https://forms.gle/SFAKzJg45CTyjhiH6
          </a>
          <br />
          <br /> После отправки заявки на тестирование, Вас переведет на
          страницу с инструкцией, а за час до указанного Вами времени для
          тестирования, с Вами свяжется специалист техподдержки через
          электронную почту , чтобы проверить готовность. Предварительное
          тестирование можно пройти с понедельника по пятницу ( 15:00 мск -
          19:00 мск ) и в субботу в любое удобное время для Вас по
          договорённости со специалистом. Всё это делается заранее для общего
          комфорта во время онлайн уроков. <br />
          <br /> Важно отметить, что пока Вы не пройдёте данную процедуру - Вы
          сможете смотреть уроки только в качестве зрителя.
        </p>
        <p className="text-center fw-bold">Желаем Вам успешного обучения!</p>
        <hr className="my-2"></hr>
      </div>
    </div>
  )
}
export default SuccesPayment
