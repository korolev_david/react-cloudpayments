import React from "react"

const InfoPayment = (props) =>{

return(
<div className="row col-md-8 col-xs-12 col-sm-10 mt-1 mx-auto mb-3 border p-4 order"
            style={{background: "#eee"}}>
            <h2>Данные об оплате</h2>
            <div className="row mt-2">
                <div className="col-7">Сумма со скидкой:</div>
                <div className="col">{props.total_cost_sale} руб</div>
            </div>
            <div className="row mt-2">
                <div className="col-7">Уже оплачено:</div>
                <div className="col">{props.paid} руб</div>
            </div>
            <div className="row mt-2">
                <div className="col-7">Осталось оплатить:</div>
                <div className="col">{props.set_paid} руб</div>
            </div>
            <div className="row mt-2 ">
                <div className="col-7">Введите сумму, руб:</div>
                <div className="col"> <input type="number"  onChange={props.paidChangeHandelr} defaultValue={props.set_paid} className="input-md form-control"/>
                </div>
            </div>
            <div className="row mt-4 justify-content-center">
                <button type="button" value="1" className="btn btn-primary" onClick={props.SetStage} style={{width: "150px"}}>Дальше</button>
            </div>

        </div>
)


}
export default InfoPayment