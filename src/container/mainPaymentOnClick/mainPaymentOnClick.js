import React, { Component } from 'react'
import { connect } from 'react-redux'
import InfoProduct from '../infoProduct/infoProduct'
import { creatDataUser, nextStage } from '../../redux/action/action'
import InfoUserAuth from '../infoUserAuth/infoUserAuth'
import LoaderCreateOrder from '../loaderCreateOrder/loaderCreateOrder'
import PaymentOnClick from '../paymentOnClick/paymentOnClick'
import SuccesPayment from '../succesPayment/succesPayment'
class MainPaymentOnClick extends Component {
  constructor(props) {
    super(props)
    this.state = {
      email: '',
      phone: '',
      validError: false,
      validText: ''
    }
    if(this.props.data.products.length !== 0)
    {
      for (let value of this.props.data.products) {
        this.state.total_cost =
          Number(this.state.total_cost ? this.state.total_cost : 0) +
          Number(value.sum)
        this.state.productId = value.idProduct
      }
    }
    
   
  }

  paymentChange = () => {
    const widget = new global.cp.CloudPayments()
    widget.charge(
      {
        // options
        publicId: "pk_7dafd51cc7bf1fdd6f965fc36e391", //id из личного кабинета
        description: this.props.dataOnClick.nameProduct, //назначение
        amount: this.state.total_cost, //сумма
        currency: 'RUB', //валюта
        invoiceId: this.props.dataOnClick.order_id, //номер заказа  (необязательно)
        accountId: this.props.dataOnClick.email, //идентификатор плательщика (необязательно)
        skin: 'modern', //, дизайн виджета
        //data: {
        //  myProp: 'myProp value' //произвольный набор параметров
        //}
      },
      (options) => {
        this.props.nextStage(4)
      },
      function (reason, options) {
        // fail
        //действие при неуспешной оплате
      },
    )
  }
  ValidPhone() {
    const pattern = new RegExp(/[-(+) //\\]/g) 
    const phone = this.state.phone.replace(pattern,'') 
    this.setState({phone:phone})
    // eslint-disable-next-line react/no-direct-mutation-state
    this.state.phone = phone;
    const regex = new RegExp(/^[+0-9\b]+$/)
    const result = regex.test(this.state.phone)
   
    if (result) {
      if (this.state.phone.length <= 15 && this.state.phone.length >= 11)
        return true
      else return false
    } else return result
  }
  ValidEmail() {
    const regex = new RegExp(
      /^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i,
    )
    const result = regex.test(this.state.email)
    return result
  }
  authUser = () => {
    if(this.ValidEmail())
    {
      if(this.ValidPhone())
      {

      }
      else
      {
        this.setState({ validError: true, validText: 'Некорректно введен телефон. Пример +71111111111'})
      }
    }
    else
    {
      this.setState({ validError: true, validText: 'Некорректно введен Email'})
    }
    if (this.ValidEmail() && this.ValidPhone()) {
      this.props.nextStage(1)
      this.props.creatDataUser(
        this.state.name,
        this.state.phone,
        this.state.email,
        this.state.productId,
      )
    } else {
     
    }
  }
  inputChangeHandler = (event) => {
    
    this.setState({
      ...this.state,
      [event.target.name]: event.target.value,
      validError: false,
    })
  }
  getPage = () => {
    switch (Number(this.props.stage)) {
      case 0:
        return (
          <div>
            <InfoProduct
              data={this.props.data}
              orderId={''}
              total_cost={this.state.total_cost}
            />
            <InfoUserAuth
              SetStage={this.authUser}
              inputChenge={this.inputChangeHandler}
              validError={this.state.validError}
              validText = {this.state.validText}
            />
          </div>
        )
      case 1:
        return (
          <div>
            <LoaderCreateOrder />
          </div>
        )
      case 2:
        return (
          <div>
            <InfoProduct
              data={this.props.data}
              orderId={'Счет № ' + this.props.dataOnClick.order_id}
              total_cost={this.props.dataOnClick.total_cost}
            />
            <PaymentOnClick
              aymentChange={this.paymentChange}
              set_paid={this.props.dataOnClick.set_paid}
              phone={this.props.dataOnClick.phone}
              email={this.props.dataOnClick.email}
              paymentChange={this.paymentChange}
            />
          </div>
        )
      case 4:
        return (
          <SuccesPayment
            description={this.props.dataOnClick.description}
            phone={this.props.dataOnClick.phone}
          />
        )

      default:
        return <div></div>
    }
  }

  render() {
    return <div className="container">{this.getPage()}</div>
  }
}
const mapDispathToProps = {
  creatDataUser,
  nextStage,
}
const mapStateToProps = (state) => ({
  data: state.apiData.data,
  dataOnClick: state.apiData.dataOnClick,
  stage: state.appData.stage,
})
export default connect(mapStateToProps, mapDispathToProps)(MainPaymentOnClick)
