const PaymentOnClick = (props) => {
  const styleButton = {
    width: '150px',
    marginTop: '5px',
  }
  

  return (
    <div
      className="row col-md-8 col-xs-12 col-sm-10 mt-1 mx-auto mb-3 border p-4 order"
      style={{ background: '#eee' }}
    >
      <h2>Оплата</h2>

      <div className="row mt-2">
        <div className="col-7">Сумма оплаты</div>
        <div className="col">{props.set_paid} руб</div>
      </div>
      <div className="row mt-2">
        <div className="col-7">Номер телефона:</div>
        <div className="col">{props.phone}</div>
      </div>
      <div className="row mt-2">
        <div className="col-7">E-mail:</div>
        <div className="col">{props.email}</div>
      </div>
      <div className="row mt-4 justify-content-around">
        <button
          type="button"
          className="btn btn-primary"
          style={styleButton}
          onClick={props.paymentChange}
        >
          Оплатить
        </button>
      </div>
    </div>
  )
}
export default PaymentOnClick
