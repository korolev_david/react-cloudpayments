import React from 'react'
// import PhoneInput from 'react-phone-input-2'
// import 'react-phone-input-2/lib/style.css'
const InfoUserAuth = (props) => {
  const styleButton = {
    width: '150px',
    marginTop: '5px',
  }
  return (
    <div
      className="row col-md-8 col-xs-12 col-sm-10 mt-1 mx-auto mb-3 border p-4 order"
      style={{ background: '#eee' }}
    >
      <h2>Ваши данные</h2>
      <div className="row mt-2">
        <div className="col">
          <input
            type="text"
            placeholder="Ф.И.О"
            name="name"
            onChange={props.inputChenge}
            className="input-md form-control"
          />
        </div>
      </div>
      <div className="row mt-2">
        <div className="col">
        {/* <PhoneInput
        name='phone'
        country={'ru'}
        style={{with:'100%'}}
        value={props.phone}
        onChange={phone =>props.inputChenge}
      /> */}
          <input
            type="tel"
            placeholder="Телефон"
            name="phone"
            onChange={props.inputChenge}
            className="input-md form-control"
            pattern="[0-9]{1}[0-9]{9}" 
            defaultValue={props.phone}
          />
        </div>
      </div>
      <div className="row mt-2">
        <div className="col">
          <input
            type="text"
            placeholder="E-mail"
            name="email"
            onChange={props.inputChenge}
            className="input-md form-control"
          />
        </div>
      </div>
      {props.validError &&  <div className="row mt-2">
        <div className="col">
          <div className="alert alert-danger" role="alert">
            {props.validText}
          </div>
        </div>
      </div>}
     

      <div className="row mt-4 justify-content-around">
        <button
          type="button"
          value="2"
          onClick={props.SetStage}
          className="btn btn-primary"
          style={styleButton}
        >
          Дальше
        </button>
      </div>
    </div>
  )
}
export default InfoUserAuth
