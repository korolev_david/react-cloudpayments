import React from "react";
const InfoProduct = (props)=>
{  
    return ( 
    <div className="row col-md-8 col-xs-12 col-sm-10 mt-5 mx-auto mb-3 border p-4 order" style={{background: "#eee"}}>
        <h2 className="display-7">Данные о заказе</h2>
        <p className="lead">{props.orderId}</p>
        <hr className="my-2"></hr>
        {
        props.data.products.map((item,i)=>{
            
            return <div key={i} className="row">
            <div className="col-1">
               {i+1}.
            </div>
            <div className="col">
                {item.productName}
            </div>
            <div className="col">
                {item.sum} руб.
            </div>
        </div>
        }
        )}
        <hr className="my-2"></hr>
        <div className="row text-end mt-2">
            <p>Итог: {props.total_cost} руб.</p>
        </div>
    </div>
)

}
export default InfoProduct