import React from 'react'
import { connect } from 'react-redux'
import calsses from './loaderCreateOrder.module.css'
const LoaderCreateOrder = ({ syncProps }) => {
  return (
    <div className={calsses.containerLd}>
        
      <svg
        className={calsses.loaderLd}
        xmlns="http://www.w3.org/2000/svg"
        viewBox="0 0 340 340"
      >
        <circle cx="170" cy="170" r="160" stroke="#0d6efd" />
        <circle cx="170" cy="170" r="135" stroke="#dee2e6" />
        <circle cx="170" cy="170" r="110" stroke="#0d6efd" />
        <circle cx="170" cy="170" r="85" stroke="#dee2e6" />
      </svg>
      <p>{syncProps}</p>
    </div>
  )
}
const mapStateToProps = (state) => {
    return {
        syncProps: state.appData.textLoader
    }
  }
export default connect(mapStateToProps, null)(LoaderCreateOrder)
