import React from "react"

const InfoUser =(props)=>
{
    const styleButton ={
        width: "150px",
        marginTop:"5px"
    }
    return(

        <div className="row col-md-8 col-xs-12 col-sm-10 mt-1 mx-auto mb-3 border p-4 order"
            style={{background: "#eee"}}>
            <h2>Ваши данные</h2>
            <div className="row mt-2">
                <div className="col">
                    <div className="col-7">Номер телефона:</div>
                    <input type="text" defaultValue={props.phone} readOnly className="input-md form-control"/>
                </div>
            </div>
            <div className="row mt-2">
                <div className="col">
                    <div className="col-7">E-mail:</div>
                    <input type="text" defaultValue={props.email} readOnly className="input-md form-control"/>
                </div>
            </div>
            <div className="row mt-2">
                <div className="col">
                    <div className="col-7">ФИО:</div>
                    <input type="text" defaultValue={props.name} readOnly className="input-md form-control"/>
                </div>

            </div>

            <div className="row mt-4 justify-content-around">
                <button type="button" value="0" onClick={props.SetStage} className="btn btn-secondary " style={styleButton}>Назад</button>
                <button type="button" value="2" onClick={props.SetStage} className="btn btn-primary" style={styleButton}>Дальше</button>
            </div>

        </div>
    )

}
export default InfoUser