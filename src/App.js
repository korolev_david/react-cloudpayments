import { connect } from 'react-redux'
import { getDataOrder, getProductData } from './redux/action/action'
import React from 'react'
import { useLocation } from 'react-router-dom'

import { useRouters } from './router/router'
function App(asyncProps) {
  const location = useLocation()
 
  if (location.pathname === '/' && asyncProps.data.length === 0 && asyncProps.error === false)
    asyncProps.getDataOrder()   
  if (location.pathname === '/product' && asyncProps.data.length === 0 && asyncProps.errorProduct === false)
    asyncProps.getProductData()
  
 
  const router = useRouters(asyncProps)
  return <div>{router}</div>
  
}
const mapDispathToProps = {
  getDataOrder,
  getProductData,
}
const mapStateToProps = (state) => ({
  error: state.appData.error,
  loader: state.appData.loader,
  data: state.apiData.data,
  errorProduct: state.appData.errorProduct
})
export default connect(mapStateToProps, mapDispathToProps)(App)
